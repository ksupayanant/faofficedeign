<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Web Profile">
    <meta name="author" content="Kornkrit Supayanant">

    <title>FA Office Design</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <link rel="stylesheet" href="vendor/superscroolrama/css/normalize.css" type="text/css">
    <link rel="stylesheet" href="vendor/superscroolrama/css/style.css" type="text/css">

    <!-- Theme CSS -->
    <!-- <link href="css/creative.min.css" rel="stylesheet"> -->
    <link href="css/creative.css" rel="stylesheet">
    <link href="css/fa-office.css" rel="stylesheet">
</head>

<body>
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">FA</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a class="page-scroll" href="#page-top">Home</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
<div class="fa-office-banner" data-ride="carousel">
    <div class="carousel-inner">
        <img src="img/main-service/land/land.jpg" class="banner-image" />
        <div class="container text-center">
            <div class="carousel-caption">
                <div class="container text-center">
                    <span class="banner-title">FA LANDSCAPE DESIGN</span>
                    <h1>บริการออกแบบภูมิทัศน์สำนักงาน</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<section id="main-service" class="white-background">
    <div class="container">
        <div class="row">
            <div class="text-center">
                <h1>Our Packages</h1>
                <h2>แพ็คเกจออกแบบสำนักงานของเรา</h2>
                <br/>
            </div>

            <!-- Carousel -->
            <div id="main-service-carousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#main-service-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#main-service-carousel" data-slide-to="1"></li>
                    <li data-target="#main-service-carousel" data-slide-to="2"></li>
                    <li data-target="#main-service-carousel" data-slide-to="3"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="img/main-service/land/unique-a.jpeg" class="caption-image">
                        <div class="text-center">
                            <div class="caption-box pull-right">
                                <div class="container">
                                    <h1>Collection Package</h1>
                                    <hr>
                                    <p class="text-center">
                                        ให้การตกแต่งภูมิทัศน์และการออกแบบมีคุณภาพและความสวยงามยิ่งขึ้น ด้วยบริการออกแบบและการสำรวจ เพื่อตอบสนองต่อความต้องการและแก้ไขปัญหาข้อจำกัดของพื้นที่ได้อย่างเหมาะสม
                                    </p>
                                        <span>
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-sm-2 center-block">
                                                    <i class="fa fa-3x fa-lightbulb-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-sm-9 text-left">
                                                    <h2>New Office</h2>
                                                    <h4>ตกแต่งภายในสำนักงานใหม่</h4>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <i class="fa fa-3x fa-paint-brush" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-sm-9 text-left">
                                                    <h2>Renovate Office</h2>
                                                    <h4>ปรับปรุงภายในสำนักงานเดิม</h4>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <a class="btn btn-md button-transparent">รายละเอียด ></a>
                                            </div>
                                        </div>
                                        </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <img src="img/main-service/land/unique-b.jpg" class="caption-image">
                        <div class="text-center">
                            <div class="caption-box pull-right">
                                <div class="container">
                                    <h1>Unique A</h1>
                                    <hr>
                                    <p class="text-center">
                                        ให้การตกแต่งภูมิทัศน์และการออกแบบมีคุณภาพและความสวยงามยิ่งขึ้น ด้วยบริการออกแบบและการสำรวจ เพื่อตอบสนองต่อความต้องการและแก้ไขปัญหาข้อจำกัดของพื้นที่ได้อย่างเหมาะสม
                                    </p>
                                        <span>
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-sm-2 center-block">
                                                    <i class="fa fa-3x fa-lightbulb-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-sm-9 text-left">
                                                    <h2>New Office</h2>
                                                    <h4>ตกแต่งภายในสำนักงานใหม่</h4>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <i class="fa fa-3x fa-paint-brush" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-sm-9 text-left">
                                                    <h2>Renovate Office</h2>
                                                    <h4>ปรับปรุงภายในสำนักงานเดิม</h4>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <a class="btn btn-md button-transparent">รายละเอียด ></a>
                                            </div>
                                        </div>
                                        </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <img src="img/main-service/land/unique-c.jpeg" class="caption-image">
                        <div class="text-center">
                            <div class="caption-box pull-right">
                                <div class="container">
                                    <h1>Unique B</h1>
                                    <hr>
                                    <p class="text-center">
                                        ให้การตกแต่งภูมิทัศน์และการออกแบบมีคุณภาพและความสวยงามยิ่งขึ้น ด้วยบริการออกแบบและการสำรวจ เพื่อตอบสนองต่อความต้องการและแก้ไขปัญหาข้อจำกัดของพื้นที่ได้อย่างเหมาะสม
                                    </p>
                                        <span>
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-sm-2 center-block">
                                                    <i class="fa fa-3x fa-lightbulb-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-sm-9 text-left">
                                                    <h2>New Office</h2>
                                                    <h4>ตกแต่งภายในสำนักงานใหม่</h4>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <i class="fa fa-3x fa-paint-brush" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-sm-9 text-left">
                                                    <h2>Renovate Office</h2>
                                                    <h4>ปรับปรุงภายในสำนักงานเดิม</h4>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <a class="btn btn-md button-transparent">รายละเอียด ></a>
                                            </div>
                                        </div>
                                        </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <img src="img/main-service/land/unique-d.jpg" class="caption-image">
                        <div class="text-center">
                            <div class="caption-box pull-right">
                                <div class="container">
                                    <h1>Unique C</h1>
                                    <hr>
                                    <p class="text-center">
                                        ให้การตกแต่งภูมิทัศน์และการออกแบบมีคุณภาพและความสวยงามยิ่งขึ้น ด้วยบริการออกแบบและการสำรวจ เพื่อตอบสนองต่อความต้องการและแก้ไขปัญหาข้อจำกัดของพื้นที่ได้อย่างเหมาะสม
                                    </p>
                                        <span>
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-sm-2 center-block">
                                                    <i class="fa fa-3x fa-lightbulb-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-sm-9 text-left">
                                                    <h2>New Office</h2>
                                                    <h4>ตกแต่งภายในสำนักงานใหม่</h4>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <i class="fa fa-3x fa-paint-brush" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-sm-9 text-left">
                                                    <h2>Renovate Office</h2>
                                                    <h4>ปรับปรุงภายในสำนักงานเดิม</h4>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <a class="btn btn-md button-transparent">รายละเอียด ></a>
                                            </div>
                                        </div>
                                        </span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Controls -->
                <a class="carousel-control" href="#main-service-carousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right-control carousel-control" href="#main-service-carousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div><!-- /carousel -->
        </div>
    </div>
</section>
<section>
    <div class="text-center">
        <h1>Select Packages</h1>
        <h2>เลือกแพ็คเกจที่เหมาะกับสำนักงานคุณ</h2>
        <br/>
    </div>
    <div class="container">
        <table class="table table-bordered text-center compare-price">
            <thead>
            <tr>
                <td class="active" style="width: 25%;"><h3>Scope</h3></td>
                <th class="text-center table-header" style="width: 18.5%;"><h3>Land A</h3><h4>Package</h4></th>
                <th class="text-center table-header" style="width: 18.5%;"><h3>Land B</h3><h4>Package</h4></th>
                <th class="text-center table-header" style="width: 18.5%;"><h3>Land C</h3><h4>Package</h4></th>
                <th class="text-center table-header" style="width: 18.5%;"><h3>Land D</h3><h4>Package</h4></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th><h4>การให้คำปรึกษา <span class="pull-right"><i class="fa fa-info-circle info-box" aria-hidden="true">
                                <span class="info-span">การให้คำปรึกษา : เป็นผู้ช่วยเสริมของโครงการที่สามารถให้คำแนะนำ ข้อเสนอแนะต่างๆในแต่ละขั้นตอนของโครงการ เพื่อให้ลูกค้าได้รับทราบข้อมูลหรือแนวทางที่เป็นประโยชน์ในการพิจารณาและตัดสินใจเรื่องๆต่างได้อย่างเหมาะสม</span></i></span></h4>
                <td>✔</td>
                <td>✔</td>
                <td>✔</td>
                <td>✔</td>
            </tr>
            <tr class="active">
                <th><h4>การบริหารโครงการ<span class="pull-right"><i class="fa fa-info-circle info-box" aria-hidden="true">
                                <span class="info-span">การบริหารโครงการ : เป็นผู้ช่วยหลักของโครงการที่สามารถทำให้ภาพรวมของงานในโครงการทั้งหมดบรรลุเป้าหมายได้ตามความต้องการของลูกค้า ทั้งเรื่องขอบเขต,คุณภาพ,เวลาและงบประมาณของโครงการ โดยการวางแผน, ติดตาม ผู้เกี่ยวข้องในงานต่างๆ ให้ดำเนินการได้อย่างถูกต้องเหมาะสมสอดคล้องกัน</span></i></span></h4></th>
                <td></td>
                <td></td>
                <td>✔</td>
                <td>✔</td>
            </tr>
            <tr>
                <th><h4>การสำรวจและประเมิน<span class="pull-right"><i class="fa fa-info-circle info-box" aria-hidden="true">
                                <span class="info-span">การสำรวจและประเมิน : เป็นผู้ช่วยเสริมของโครงการ ที่สามารถทำให้เห็นข้อมูลที่ชัดเจนถึง สภาพพื้นที่ก่อสร้างและข้อจำกัดต่างๆ โดยการลงพื้นที่ สำรวจ เก็บข้อมูลอย่างรายละเอียด เพื่อเป็นข้อมูลประกอบการตัดสินใจที่ถูกต้อง</span></i></span></h4></th>
                <td></td>
                <td>✔</td>
                <td>✔</td>
                <td>✔</td>
            </tr>
            <tr class="active">
                <th><h4>การออกแบบ<span class="pull-right"><i class="fa fa-info-circle info-box" aria-hidden="true">
                                <span class="info-span">การออกแบบ : เป็นผู้ช่วยหลักของงานช่วงออกแบบโดยเฉพาะ ที่สามารถทำให้ลูกค้าได้เห็นภาพสุดท้ายของโครงการและรูปแบบการใช้งาน ความสวยงามรวมถึงคุณสมบัติของวัสดุและอุปกรณ์ต่างๆก่อนก่อสร้างได้ โดยการออกแบบให้สอดคล้องกับความต้องการของลูกค้า</span></i></span></h4></th>
                <td>✔</td>
                <td>✔</td>
                <td>✔</td>
                <td>✔</td>
            </tr>
            <tr>
                <th><h4>การบริหารก่อสร้าง<span class="pull-right"><i class="fa fa-info-circle info-box" aria-hidden="true">
                                <span class="info-span text-justify">การบริหารงานก่อสร้าง : เป็นผู้ช่วยหลักของงานช่วงก่อสร้างโดยเฉพาะ ที่สามารถทำให้ ผู้รับเหมาทำงานได้อย่างถูกต้อง เหมาะสมตามหลักการและแบบที่ผู้ออกแบบได้กำหนดไว้ ทั้งเรื่องขอบเขต,คุณภาพ,เวลาและงบประมาณของงานก่อสร้าง โดยการ ควบคุม,ติดตาม,ตรวจสอบผู้รับเหมาอย่างใกล้ชิด</span></i></span></h4></th>
                <td></td>
                <td></td>
                <td></td>
                <td>✔</td>
            </tr>
            <tr class="active">
                <th><h4>การรับเหมาก่อสร้าง</h4></th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th></th>
                <td><h4  class="text-yellow-color"><a href="#">รายละเอียด ></a></h4></td>
                <td><h4  class="text-yellow-color"><a href="#">รายละเอียด ></a></h4></td>
                <td><h4  class="text-yellow-color"><a href="#">รายละเอียด ></a></h4></td>
                <td><h4  class="text-yellow-color"><a href="#">รายละเอียด ></a></h4></td>
            </tr>
            <tr class="active">
                <th class="text-center">
                    <i class="fa fa-3x fa-lightbulb-o" aria-hidden="true"></i>
                    <h2>New Office</h2>
                    <h4>ตกแต่งภายในสำนักงานใหม่</h4>
                </th>
                <td><h4>เริ่มต้น<div style="padding: 5%;">฿620</div>ราคาต่อตร.ม.</h4><a class="btn btn-choose">เลือก</a></td>
                <td><h4>เริ่มต้น<div style="padding: 5%;">฿645</div>ราคาต่อตร.ม.</h4><a class="btn btn-choose">เลือก</a></td>
                <td><h4>เริ่มต้น<div style="padding: 5%;">฿1,400</div>ราคาต่อตร.ม.</h4><a class="btn btn-choose">เลือก</a></td>
                <td><h4>เริ่มต้น<div style="padding: 5%;">฿1,430</div>ราคาต่อตร.ม.</h4><a class="btn btn-choose">เลือก</a></td>
            </tr>
            <tr class="active">
                <th class="text-center">
                    <i class="fa fa-3x fa-paint-brush" aria-hidden="true"></i>
                    <h2>Renovate Office</h2>
                    <h4>ปรับปรุงภายในสำนักงานเดิม</h4>
                </th>
                <td><h4>เริ่มต้น<div style="padding: 5%;">฿880</div>ราคาต่อตร.ม.</h4><a class="btn btn-choose">เลือก</a></td>
                <td><h4>เริ่มต้น<div style="padding: 5%;">฿1,340</div>ราคาต่อตร.ม.</h4><a class="btn btn-choose">เลือก</a></td>
                <td><h4>เริ่มต้น<div style="padding: 5%;">฿1,975</div>ราคาต่อตร.ม.</h4><a class="btn btn-choose">เลือก</a></td>
                <td><h4>เริ่มต้น<div style="padding: 5%;">฿2,275</div>ราคาต่อตร.ม.</h4><a class="btn btn-choose">เลือก</a></td>
            </tr>
            <tbody>
        </table>
    </div>
</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>-->
<script src="vendor/scrollreveal/scrollreveal.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="vendor/superscroolrama/js/greensock/TweenMax.min.js"></script>
<script src="vendor/superscroolrama/js/jquery.superscrollorama.js"></script>
<!-- Theme JavaScript -->
<script src="js/runcode.js"></script>
<script src="js/creative.js"></script>
<script src="https://repl.it/lib/api.js" type="text/javascript" charset="utf-8"></script>


</body>

</html>