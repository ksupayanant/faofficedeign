<!doctype html>
<html>
<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>FA OFFICE DESIGN</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <link rel="stylesheet" href="vendor/superscroolrama/css/normalize.css" type="text/css">
    <link rel="stylesheet" href="vendor/superscroolrama/css/style.css" type="text/css">

    <!-- Theme CSS -->
    <link href="css/creative.css" rel="stylesheet">
    <link href="css/fa-main.css" rel="stylesheet">

</head>
<body class="main-body" id="page-top">
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">FA</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a class="page-scroll" href="#page-top">Home</a>
                </li>
                <li>
                    <a class="page-scroll" href="#about-us">About Us</a>
                </li>
                <li class="dropdown submenu">
                    <a>Service</a>
                    <ul class="dropdown-menu menu-drop">
                        <li>
                            <a class="page-scroll" href="#main-service">Main Service</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#object-service">Object Service</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#product-design">Product Design</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="page-scroll" href="#why-fa">Why FA</a>
                </li>
                <li>
                    <a class="page-scroll" href="#customer">Customer</a>
                </li>
                <li>
                    <a class="page-scroll" href="#contact-us">Contact Us</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <img src="img/building.jpg" class="carousel-image">
            <div class="container text-center">
                <div class="carousel-caption">
                    <div class="container text-center">
                        <h1>FA OFFICE DESIGN</h1>
                        <h1>บริการออกแบบสำนักงาน</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="item">
            <img src="img/office.jpg" class="carousel-image">
            <div class="container text-center">
                <div class="carousel-caption">
                    <div class="container text-left">
                        <h1>Good Environment Good Job !!</h1>
                        <h2>บริการออกแบบสำนักงาน</h2>
                        <p>ตกแต่งภายในออฟฟิศ ออกแบบอาคาร<br/>สำนักงาน ออกแบบภูมิทัศน์สำนักงาน<br/> ออกแบบชิ้นงาน ผลิตภัณฑ์มุม จาก FA</p>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<section id="about-us" class="white-background">
    <div class="container">
        <div class="row">
            <div class="col-md-5 text-justify">
                <span class="text-slide-in"><h1 id="fly-it-about-us">ABOUT US</h1></span>
                <p>
                    FA คือ ผู้ให้บริการออกแบบและบริหารพื้นที่สำนักงานโดยเฉพาะ ที่จะช่วยคุณในการออกแบบสำนักงานที่ตอบโจทย์ความต้องการ เราพร้อมที่จะนำเสนอพื้นที่ทำงานในรูปแบบใหม่ๆมากมาย เพื่อส่งมอบพื้นที่ที่จะเป็นประโยชน์สำหรับสำนักงานของคุณ ด้วยแนวคิด “ออฟฟิศที่สมดุล” ที่ทุกพื้นที่ต้องสร้างสมดุลทั้งด้านประสิทธิภาพการใช้งานและสุนทรีภาพที่เกิดจากการใช้พื้นที่ เพื่อเป็นส่วนหนึ่งในการร่วมสร้างสำนักงานที่เต็มเปี่ยมไปด้วยแรงบันดาลใจ เพราะเราเชื่อว่าพื้นที่ทำงานที่ดีจะช่วยเพิ่มประสิทธิภาพและเพิ่มความสุขในการทำงาน ทำให้ผู้คนมีคุณภาพชีวิตที่ดีขึ้น เพื่อมีพลังในการสร้างสรรค์งานที่ดีเพื่อผู้คนอื่นๆต่อไป
                </p>
                <input class="toggle-box" id="identifier-1" type="checkbox" >
                <label for="identifier-1">อ่านเพิ่ม</label>
                <div><p>ที่ทุกพื้นที่ต้องสร้างสมดุลทั้งด้านสุนทรีภาพที่เกิดจากการใช้พื้นที่และประสิทธิภาพการใช้งาน เพื่อเป็นส่วนหนึ่งในการร่วมสร้างสำนักงานที่เต็มเปี่ยมไปด้วยแรงบันดาลใจ เพราะเราเชื่อว่าพื้นที่ทำงานที่ดีจะช่วยเพิ่มประสิทธิภาพและเพิ่มความสุขในการทำงาน ทำให้ผู้คนมีคุณภาพชีวิตที่ดีขึ้น ให้มีพลังในการสร้างสรรค์งานที่ดีเพื่อผู้คนอื่นๆต่อไป</p></div>
            </div>
            <div class="col-md-5 pull-right">
                <img src="img/about-us/about-us.png" class="img-responsive" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 align-middle">
                <br /><br />
                <img src="img/about-us/balance.png" class="img-responsive" />
            </div>
            <div class="col-md-5 pull-right text-justify">
                <span class="sr-icons"><h1 id="fly-it-design">DESIGN</h1></span>
                <p>
                    จากแนวคิด "ออฟฟิศที่สมดุล" สู่งานออกแบบ
                    <ul>
                        <li>
                <p>ประสิทธิภาพการทำงาน : สำรวจ วิเคราะห์รูปแบบการใช้งานพื้นที่ของแต่ละองค์กรแล้วทำการออกแบบพื้นที่ต่างๆให้เกิดสภาวะต่างๆที่สอดคล้องและสนับสนุนการทำงานอย่างสร้างสรรค์มากยิ่งขึ้น</p>
                <input class="toggle-box" id="identifier-2" type="checkbox" >
                <label for="identifier-2">อ่านเพิ่ม</label>
                <div><p>บ่งบอกตัวตน  แบรนด์และองค์กรของคุณให้ทุกคนรับรู้ จดจำ ส่งผ่านไปถึงพนักงานของคุณ ลูกค้าของคุณ และทุกๆคน เพื่อสร้างความประทับใจสร้างประสบการณ์ใหม่ๆ สร้างคุณภาพชีวิตที่ดีขึ้น และสร้างความสุขในการทำงาน</p></div>

                </li>
                <li>
                    <p>สุนทรียภาพการใช้ชีวิต : การออกแบบและตกแต่งพื้นที่ให้สะท้อนถึงภาพลักษณ์ขององค์กรและส่งเสริมรูปแบบวิถีชีวิตของคนที่ใช้พื้นที่ให้มีความสุขกับการทำงานมากยิ่งขึ้น</p>
                    <input class="toggle-box" id="identifier-3" type="checkbox" >
                    <label for="identifier-3">อ่านเพิ่ม</label>
                    <div><p>เพื่อให้มั่นใจว่าการจัดการทุกพื้นที่ต้องเกิด ประสิทธิภาพและสามารถวัดผลได เพื่อสร้างความ ความพึงพอใจให้กับผู้ใช้งานและช่วยส่งเสริมการ ทำงานให้เกิดประสิทธิภาพสูงสุด</p></div>
                </li>
                </ul>
                </p>

            </div>
        </div>
        <div class="row">
            <div class="col-md-5 text-justify">
                <span class="sr-icons"><h1 id="fly-it-service">SERVICES & PRODUCT</h1></span>
                <p>
                    “เรามุ่งมั่นในการออกแบบสร้างสรรค์บริการและผลิตภัณฑ์ที่มีคุณค่าเพื่อคุณภาพชีวิตที่ดีขึ้นของผู้คนในสังคม” ด้วยงานออกแบบ สู่รูปแบบบริการและผลิตภัณฑ์ของเรา
                </p>
                <input class="toggle-box" id="identifier-4" type="checkbox" >
                <label for="identifier-4">อ่านเพิ่ม</label>
                <div>
                    <p>
                        <ul class="list-circle">
                            <li>
                    <p>บริการหลัก (Main Service) : เหมาะสำหรับสำนักงานที่ต้องการออกแบบหรือก่อสร้างที่ครบวงจร  โดยมีโปรแกรมความต้องการในเบื้องต้นที่ชัดเจนแล้ว</p>
                    </li>
                    <li>
                        <p>บริการเสริม(Pocket Service) : เหมาะสำหรับสำนักงานที่ต้องการสร้างหรือปรับปรุงพื้นที่ใหม่  เพื่อตอบโจทย์ความต้องการเฉพาะเรื่อง หรือใช้บริการเพื่อเป็นแนวทางประกอบการตัดสินใจ ด้วยรูปแบบบริการที่กระชับ ง่าย สะดวกและประหยัดยิ่งขึ้น</p>
                    </li>
                    <li>
                        <p>บริการออกแบบชิ้นงาน (Object Design Service) เหมาะสำหรับสำนักงานที่ต้องการบริการออกแบบเพียงบางส่วนในพื้นที่สำนักงาน เพื่อนำไปใช้ประกอบการก่อสร้าง มี 2 รูปแบบบริการคือ</p>
                        <ul>
                            <li><p><i class="fa fa-map-marker" aria-hidden="true"></i> Onsite service ออกแบบลงพื้นที่หน้างาน</p></li>
                            <li><p><i class="fa fa-globe" aria-hidden="true"></i> Online Service ออกแบบออนไลน์ (ใหม่) ช่วยประหยัดได้ Save10-30%</p></li>
                        </ul>
                    </li>
                    <li>
                        <p>ผลิตภัณฑ์ (Product ) มุม : MOOM By FA เหมาะสำหรับสำนักงานที่ต้องการพื้นที่กิจกรรมกึ่งสำเร็จรูป โดยสามารถจะเลือกมุม: MOOM ที่คุณต้องการนำไปจัดวางในพื้นที่สำนักงานของคุณได้ทันที (เร็วๆนี้)</p>
                    </li>
                    </ul>
                    </p>
                    <p>ปรึกษาเราหากคุณไม่แน่ใจว่าจะเลือกบริการหรือแพ็คเกจใด ที่จะคุ้มค่าและเหมาะกับโครงการของคุณ  เพียงแจ้งความต้องการ แจ้งปัญหาหรือข้อจำกัดต่างๆที่คุณมี  เรายินดีประเมินให้ฟรี ได้ที่นี่</p>
                    <p>คุณสามารถเสนอความคิดเห็นเพื่อแนะนำ ติชมหรือเพื่อการปรับปรุงและพัฒนาบริการและผลิตภัณฑ์ ได้ที่นี่</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="main-service" class="yellow-background">
    <div class="container">
            <span class="container pull-left">
                <i class="fa fa-map fa-5x sr-contact"></i>
            </span>
        <div class="container">
            <h1>Main Service</h1>
            <h1>บริการหลัก</h1>
        </div>
    </div>
    <div class="container">
        <div class="row centered">
            @foreach($mainInfos as $mainInfo)
                <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                    <div class="card card-inverse card-info">
                        <img class="card-img-top" src="{{ $mainInfo->image_cover }}">
                        <div class="card-block">
                            <h3 class="card-title">{{ $mainInfo->main_title_en }}</h3>
                            {{ $mainInfo->main_title_th }}
                            <div class="card-text">
                                {{ $mainInfo->description }}
                            </div>
                        </div>
                        <div class="card-footer">
                            <a href="main-service?main-id={{ $mainInfo->id }}&main-title={{ $mainInfo -> main_title_en }}" class="btn btn-package">PACKAGES</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<section id="pocket-service" class="white-background">
    <div class="container">
        <h1>Pocket Design</h1>
        <div class="row">
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <img class="card-img-top" src="img/pocket/first-idea.jpeg">
                    <h3 class="card-title">FIRST IDEA</h3>
                    <div class="card-text">
                        คิดก่อนสร้าง
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn text-center"> > </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <img class="card-img-top" src="img/pocket/plan.jpg">
                    <h3 class="card-title">PLAN</h3>
                    <div class="card-text">
                        จัดผังพื้นที่
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn text-center"> > </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <img class="card-img-top" src="img/pocket/3d-concept.png">
                    <h3 class="card-title">3D CONCEPT</h3>
                    <div class="card-text">
                        แนวคิด 3 มิติ
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn text-center"> > </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <img class="card-img-top" src="img/pocket/choose.jpeg">
                    <h3 class="card-title">CHOOSE & SHOP</h3>
                    <div class="card-text">
                        จัดหาจัดซื้อ
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn text-center"> > </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="object-service" class="black-background white-text">
    <div class="container">
        <div class="container">
            <h1>OBJECT DESIGN SERVICE</h1>
            <span>บริการออกแบบชิ้นงาน   คุณสามารถเลือกบริการออกแบบชิ้นงานอื่นๆ เพิ่มเติมในสำนักงาน  นอกเหนือจากบริการหลัก สำหรับ 2 แพ็คเกจคือ COLLECTION (เร็วๆนี้) และ UNIQUE ซึ่งรองรับบริการใหม่ !! ออกแบบออนไลน์ ( Online Design ) Save 10-30 % </span>
            <hr>
        </div>
    </div>
    <div class="container">
        <br />
        <div class="row">
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <i class="fa fa-map fa-5x sr-contact"></i>
                    <h3 class="card-title">FACADE DESIGN</h3>
                    <div class="card-text">
                        ออกแบบหน้ากากอาคาร<br />
                        เริ่มต้น ฿ 320 /ตร.ม.
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn"> > </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <i class="fa fa-map fa-5x sr-contact"></i>
                    <h3 class="card-title">FACADE DESIGN</h3>
                    <div class="card-text">
                        ออกแบบหน้ากากอาคาร<br />
                        เริ่มต้น ฿ 320 /ตร.ม.
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn"> > </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <i class="fa fa-map fa-5x sr-contact"></i>
                    <h3 class="card-title">FACADE DESIGN</h3>
                    <div class="card-text">
                        ออกแบบหน้ากากอาคาร<br />
                        เริ่มต้น ฿ 320 /ตร.ม.
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn"> > </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <i class="fa fa-map fa-5x sr-contact"></i>
                    <h3 class="card-title">FACADE DESIGN</h3>
                    <div class="card-text">
                        ออกแบบหน้ากากอาคาร<br />
                        เริ่มต้น ฿ 320 /ตร.ม.
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn"> > </a>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <i class="fa fa-map fa-5x sr-contact"></i>
                    <h3 class="card-title">FACADE DESIGN</h3>
                    <div class="card-text">
                        ออกแบบหน้ากากอาคาร<br />
                        เริ่มต้น ฿ 320 /ตร.ม.
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn"> > </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <i class="fa fa-map fa-5x sr-contact"></i>
                    <h3 class="card-title">FACADE DESIGN</h3>
                    <div class="card-text">
                        ออกแบบหน้ากากอาคาร<br />
                        เริ่มต้น ฿ 320 /ตร.ม.
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn"> > </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <i class="fa fa-map fa-5x sr-contact"></i>
                    <h3 class="card-title">FACADE DESIGN</h3>
                    <div class="card-text">
                        ออกแบบหน้ากากอาคาร<br />
                        เริ่มต้น ฿ 320 /ตร.ม.
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn"> > </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <i class="fa fa-map fa-5x sr-contact"></i>
                    <h3 class="card-title">FACADE DESIGN</h3>
                    <div class="card-text">
                        ออกแบบหน้ากากอาคาร<br />
                        เริ่มต้น ฿ 320 /ตร.ม.
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn"> > </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <br/>
        <div class="container">
            <span>บริการออกแบบชิ้นงาน   คุณสามารถเลือกบริการออกแบบชิ้นงานอื่นๆ เพิ่มเติมในสำนักงาน  นอกเหนือจากบริการหลัก สำหรับ 2 แพ็คเกจคือ COLLECTION (เร็วๆนี้) และ UNIQUE ซึ่งรองรับบริการใหม่ !! ออกแบบออนไลน์ ( Online Design ) Save 10-30 % </span>
            <hr>
        </div>
    </div>
    <div class="container">
        <br />
        <div class="row">
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <i class="fa fa-map fa-5x sr-contact"></i>
                    <h3 class="card-title">FACADE DESIGN</h3>
                    <div class="card-text">
                        ออกแบบหน้ากากอาคาร<br />
                        เริ่มต้น ฿ 320 /ตร.ม.
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn"> > </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <i class="fa fa-map fa-5x sr-contact"></i>
                    <h3 class="card-title">FACADE DESIGN</h3>
                    <div class="card-text">
                        ออกแบบหน้ากากอาคาร<br />
                        เริ่มต้น ฿ 320 /ตร.ม.
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn"> > </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <i class="fa fa-map fa-5x sr-contact"></i>
                    <h3 class="card-title">FACADE DESIGN</h3>
                    <div class="card-text">
                        ออกแบบหน้ากากอาคาร<br />
                        เริ่มต้น ฿ 320 /ตร.ม.
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn"> > </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <i class="fa fa-map fa-5x sr-contact"></i>
                    <h3 class="card-title">FACADE DESIGN</h3>
                    <div class="card-text">
                        ออกแบบหน้ากากอาคาร<br />
                        เริ่มต้น ฿ 320 /ตร.ม.
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn"> > </a>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <i class="fa fa-map fa-5x sr-contact"></i>
                    <h3 class="card-title">FACADE DESIGN</h3>
                    <div class="card-text">
                        ออกแบบหน้ากากอาคาร<br />
                        เริ่มต้น ฿ 320 /ตร.ม.
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn"> > </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <i class="fa fa-map fa-5x sr-contact"></i>
                    <h3 class="card-title">FACADE DESIGN</h3>
                    <div class="card-text">
                        ออกแบบหน้ากากอาคาร<br />
                        เริ่มต้น ฿ 320 /ตร.ม.
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn"> > </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <i class="fa fa-map fa-5x sr-contact"></i>
                    <h3 class="card-title">FACADE DESIGN</h3>
                    <div class="card-text">
                        ออกแบบหน้ากากอาคาร<br />
                        เริ่มต้น ฿ 320 /ตร.ม.
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn"> > </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                <div class="card card-inverse card-info object-box text-center brown-text">
                    <i class="fa fa-map fa-5x sr-contact"></i>
                    <h3 class="card-title">FACADE DESIGN</h3>
                    <div class="card-text">
                        ออกแบบหน้ากากอาคาร<br />
                        เริ่มต้น ฿ 320 /ตร.ม.
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info circle-btn"> > </a>
                    </div>
                </div>
            </div>
        </div>
        <br />

    </div>
</section>
<section id="product-design" class="yellow-background">
    <div class="container">
            <span class="container pull-left">
                <i class="fa fa-map fa-5x sr-contact"></i>
            </span>
        <div class="container">
            <h1>Product Design</h1>
        </div>
        <div class="container text-center">
            <h1>MOOM</h1>
            <h3>By FA</h3>
            เพื่อเติมเต็ม  “มุม ” หนึ่ง “มุม” ใดในออฟฟิศของคุณ

            รู้จักมุม : MOOM <br />

            MOOM BY FA คือ พื้นที่กิจกรรมสำเร็จรูปที่ออกแบบและสร้างสรรค์ขึ้นมาโดยนักออกแบบของเรา  คุณสามารถจะนำมุม : MOOM ไปจัดวางในพื้นที่ของคุณได้ทันที  สำหรับรองรับกิจกรรมที่หลากหลายภายในออฟฟิศสมัยใหม่ เพื่อให้ผู้ใช้พื้นที่ได้รับประสบการณ์ที่แตกต่างนอกเหนือจากพื้นที่ทำงานแบบเดิม  เหมาะสำหรับพื้นที่ออฟฟิศสร้างใหม่ ออฟฟิศเดิม ออฟฟิศในร้านกาแฟ Co-Working Space โฮมออฟฟิศ หรือพื้นที่อื่นๆ  อยากให้ออฟฟิศมีมุมแบบไหน  เลือกทำมุม : MOOM ในแบบที่คุณต้องการ
            ​
            <br />
            พบกับคอลเล็กชั่นใหม่ๆของมุม : MOOM ได้ที่นี่ !! เร็วๆนี้
            <br /><br />
            <div class="row">
                <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                    <div class="card card-inverse card-info object-box text-center brown-text">
                        <img class="card-img-top" src="http://success-at-work.com/wp-content/uploads/2015/04/free-stock-photos.gif">
                        <h3 class="card-title">FACADE DESIGN</h3>
                        <div class="card-text">
                            ออกแบบหน้ากากอาคาร
                        </div>
                        <div class="card-footer">
                            <a class="btn btn-info circle-btn"> > </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                    <div class="card card-inverse card-info object-box text-center brown-text">
                        <img class="card-img-top" src="http://success-at-work.com/wp-content/uploads/2015/04/free-stock-photos.gif">
                        <h3 class="card-title">FACADE DESIGN</h3>
                        <div class="card-text">
                            ออกแบบหน้ากากอาคาร
                        </div>
                        <div class="card-footer">
                            <a class="btn btn-info circle-btn"> > </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                    <div class="card card-inverse card-info object-box text-center brown-text">
                        <img class="card-img-top" src="http://success-at-work.com/wp-content/uploads/2015/04/free-stock-photos.gif">
                        <h3 class="card-title">FACADE DESIGN</h3>
                        <div class="card-text">
                            ออกแบบหน้ากากอาคาร
                        </div>
                        <div class="card-footer">
                            <a class="btn btn-info circle-btn"> > </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 mt-6">
                    <div class="card card-inverse card-info object-box text-center brown-text">
                        <img class="card-img-top" src="http://success-at-work.com/wp-content/uploads/2015/04/free-stock-photos.gif">
                        <h3 class="card-title">FACADE DESIGN</h3>
                        <div class="card-text">
                            ออกแบบหน้ากากอาคาร
                        </div>
                        <div class="card-footer">
                            <a class="btn btn-info circle-btn"> > </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="why-fa" class="white-background">
    <div class="container">
        <h1>WHY FA</h1>
        <div class="row">
            <div class="col-sm-4 col-md-4 col-lg-3">
                <i class="fa fa-map fa-5x sr-contact"></i>
                <div class="card-title">สุนทรียภาพในการใช้พื้นที่</div>
                <hr class="line-seperator">
                เราสร้างสรรค์ทุกงานออกแบบ เพื่อส่งมอบสุนทรีภาพในการใช้พื้นที่ ให้สอดคล้องเป็นอันหนึ่งอันเดียวกันกับแบรนด์และองค์กรของคุณ เพื่อสร้างความประทับใจ สร้างประสบการณ์ใหม่ๆ สร้างคุณภาพชีวิตที่ดีขึ้น  ส่งเสริมและสร้างความสุขในการทำงาน
            </div>
            <div class="col-sm-4 col-md-4 col-lg-3">
                <i class="fa fa-map fa-5x sr-contact"></i>
                <div class="card-title">ประสิทธิภาพในการใช้พื้นที่</div>
                <hr class="line-seperator">
                เราเชี่ยวชาญในการออกแบบและบริหารพื้นที่สำนักงานโดยเฉพาะ คุณจึงมั่นใจได้ว่าเราใส่ใจและให้ความสำคัญกับทุกปัจจัยในการออกแบบสำนักงาน เพื่อให้ได้สำนักงานที่มีคุณภาพและประสิทธิภาพด้วยมาตรฐานการออกแบบจาก FA
            </div>
            <div class="col-sm-4 col-md-4 col-lg-3">
                <i class="fa fa-map fa-5x sr-contact"></i>
                <div class="card-title">ช่วยแก้ปัญหา ช่วยลดความเสี่ยงในงานก่อสร้าง</div>
                <hr class="line-seperator">
                เพราะการออกแบบ คือจุดเริ่มต้นของงานก่อสร้าง ไม่ใช่แค่เรื่องความสวยงาม หน้าที่ของเราคือการออกแบบที่จะต้องคำนึงถึงปัจจัยต่างๆ เพื่อช่วยคุณในการแก้ปัญหา ช่วยลดความเสี่ยง ช่วยป้องกันความเสียหายที่อาจเกิดขึ้นในงานก่อสร้าง เพื่อให้คุณได้รับในสิ่งที่ดีที่สุดภายใต้ข้อจำกัดต่างๆ
            </div>
            <div class="col-sm-4 col-md-4 col-lg-3">
                <i class="fa fa-map fa-5x sr-contact"></i>
                <div class="card-title">หลากหลาย ง่าย สะดวก และรวดเร็ว</div>
                <hr class="line-seperator">
                หลากหลายบริการและผลิตภัณฑ์ในรูปแบบใหม่ๆเพื่อตอบสนองความต้องการ ให้สามารถเลือกใช้บริการได้ง่าย สะดวกรวดเร็วขึ้น และสามารถปรับเปลี่ยนยืดหยุ่นได้โดยมีขอบเขตบริการและสิ่งที่คุณจะได้รับชัดเจน
            </div>
        </div>
    </div>
</section>
<section id="customer" class="black-background white-text">
    <div class="container">
        <h1>CUSTOMER</h1>
        <div class="row">
            <div class="col-sm-4 col-md-4 col-lg-3">
                <i class="fa fa-map fa-5x sr-contact"></i>
                <div class="card-title">เทคโนโลยีแห่งอนาคต</div>
                <hr class="line-seperator">
                เน้นการออกแบบ ตกแต่งผสมผสานเทคโนโลยีแห่งอนาคตสำหรับสำนักงาน ให้คุณสามารถเลือกโซลูชั่น ที่เหมาะสม สอดคล้อง สำหรับธุรกิจได้สะดวก ง่าย และรวดเร็ว
            </div>
            <div class="col-sm-4 col-md-4 col-lg-3">
                <i class="fa fa-map fa-5x sr-contact"></i>
                <div class="card-title">ควบคุมงบประมาณได้</div>
                <hr class="line-seperator">
                มั่นใจ งบไม่บานปลาย ด้วยบริการออกแบบควบคู่กับการควบคุมงบประมาณ ตั้งแต่เริ่มต้นช่วยให้คุณสามารถวางแผนและตัดสินใจได้ง่าย ถูกต้อง รวดเร็วขึ้น
            </div>
            <div class="col-sm-4 col-md-4 col-lg-3">
                <i class="fa fa-map fa-5x sr-contact"></i>
                <div class="card-title">ออกแบบออนไลน์</div>
                <hr class="line-seperator">
                ทางเลือกใหม่บริการออกแบบออนไลน์*ช่วยให้คุณสามารถประหยัดค่าใช้จ่ายในการออกแบบมากกว่าเดิมถึง 30% ไม่ว่าพื้นที่ก่อสร้างจะอยู่ที่ไหน ก็ใช้บริการได้
            </div>
            <div class="col-sm-4 col-md-4 col-lg-3">
                <i class="fa fa-map fa-5x sr-contact"></i>
                <div class="card-title">เลือกใช้บริการง่าย</div>
                <hr class="line-seperator">
                หลากหลายบริการเพื่อตอบสนองความต้องการที่เฉพาะเจาะจงสำหรับสำนักงานแต่ละประเภทให้สามารถเลือกใช้บริการจากเว็บไซด์ได้ง่าย สะดวก และรวดเร็ว
            </div>
        </div>
    </div>
</section>
<section id="contact-us" class="yellow-background">
    <div class="container">
        <h1>CONTACT</h1>
        <div class="row">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-2">
                        <input type="text" class="form-control" placeholder="NAME" />

                        <input type="text" class="form-control" placeholder="EMAIL" />

                        <input type="text" class="form-control" placeholder="SUBJECT" />
                    </div>
                    <div class="col-sm-4">
                        <textarea class="form-control" rows="4" placeholder="MESSAGE"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pull-right">
            <div class="col-sm-6" style="">
                <button class="btn btn-success">Send</button>
            </div>
        </div>
    </div>
</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Plugin JavaScript -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>-->
<script src="vendor/scrollreveal/scrollreveal.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="vendor/superscroolrama/js/greensock/TweenMax.min.js"></script>
<script src="vendor/superscroolrama/js/jquery.superscrollorama.js"></script>
<!-- Theme JavaScript -->
<script src="js/runcode.js"></script>
<script src="js/creative.js"></script>
<script src="js/api.js" type="text/javascript" charset="utf-8"></script>
</html>
