<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    $mainInfo = DB::table('main_service_info')->get();
    return View::make('index')->with('mainInfos', $mainInfo);
});

Route::get('/test', function(){
    $mainInfo = DB::table('main_service_info')->get();
    return View::make('index2')->with('mainInfos', $mainInfo);
});


Route::get('/main', function () {
    return view('main-service-office');
});
