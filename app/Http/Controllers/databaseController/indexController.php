<?php

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class indexController extends Controller
{
    public function getMainServiceDetail(){
        $mainInfo = DB::table('main_service_info')->get();
        return View::make('index2')->with('mainInfos', $mainInfo);
    }
}