<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainServiceInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('main_service_info', function (Blueprint $table) {
                $table->increments('id');
                $table->string('main_title_en');
                $table->string('main_title_th');
                $table->string('description');
                $table->string('image_cover')->nullable();
                $table->tinyInteger('rec_status');
                $table->Integer('order_key');
                $table->timestamps();
            });

            DB::table('main_service_info')->insert(
                [
                    'main_title_en' => 'FA OFFICE INTERIOR',
                    'main_title_th' => 'บริการออกแบบตกแต่งภายในสำนักงาน',
                    'description' => 'สร้างสรรค์การตกแต่งภายในสำนักงานให้เกิดความสมดุลทั้งประสิทธิภาพการทำงานและสุนทรียภาพการใช้ชีวิต เพื่อสร้างสภาวะการทำงานที่เหมาะสมกับองค์กร',
                    'image_cover' => '',
                    'rec_status' => 1,
                    'order_key' => 1,
                ]);
        DB::table('main_service_info')->insert(
                [
                    'main_title_en' => 'FA OFFICE BUILDING',
                    'main_title_th' => 'บริการออกแบบอาคารสำนักงาน',
                    'description' => 'สร้างสรรค์รูปแบบอาคารสำนักงานที่สะท้อนถึงภาพลักษณ์องค์กรและคำนึงถึงความคุ้มค่าของการใช้พื้นที่อย่างมีประสิทธิภาพ พร้อมทั้งประสานเทคโนโลยีที่ทันสมัยเหมาะสมกับการใช้งานและการบำรุงรักษา',
                    'image_cover' => '',
                    'rec_status' => 1,
                    'order_key' => 2
                ]);
        DB::table('main_service_info')->insert(
                [
                    'main_title_en' => 'FA OFFICE LANDSCAPE',
                    'main_title_th' => 'บริการออกแบบภูมิทัศน์สำนักงาน',
                    'description' => 'สร้างสรรค์สภาพแวดล้อมภายนอกอาคารให้ช่วยสนับสนุนกับกิจกรรมการทำงานขององค์กรและยังส่งเสริมมุมมองที่สวยงามให้กับตัวอาคารสำนักงานมากยิ่งขึ้น',
                    'image_cover' => '',
                    'rec_status' => 1,
                    'order_key' => 3,
                ]
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_service_info');
    }
}
